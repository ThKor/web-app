package com.codehub.projectfuture.webapp;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
class WebAppApplicationTests {

	private static final Logger LOGGER = LoggerFactory.getLogger(WebAppApplicationTests.class);
	List<String> passwordsToBeHased = Arrays.asList("12345678","34872345", "12348244", "45876234","20768956");

	@Autowired
	private SecurityConfig securityConfig;

	@Test
	public void contextLoads() {
		passwordsToBeHased.forEach(password ->
				LOGGER.info("Hash value of password " + password + "is :" + securityConfig.passwordEncoder().encode(password)));

	}

}
