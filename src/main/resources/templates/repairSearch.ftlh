<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Search repairs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="./../css/dashboard.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
</head>
<body>

<#include "partials/navbar.ftlh">

<div class="container-fluid">
    <div class="row">
        <#include "partials/sidebar_repairs_create_search.ftlh">

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <form class="form-inline mb-3" action="/admin/searchRepairs" method="post">
                <label class="sr-only" for="from">From</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><span data-feather="calendar"></span></span>
                    </div>
                    <input type="text" class="form-control mr-sm-2" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" name="date_one" placeholder="From" id="from">
                </div>

                <label class="sr-only" for="to">To</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><span data-feather="calendar"></span></span>
                    </div>
                    <input type="text" class="form-control mr-sm-2" data-date-format="yyyy-mm-dd" data-provide="datepicker-inline" name="date_two" placeholder="To" id="to">
                </div>

                <label class="sr-only" for="social-number">Tax Registry Number</label>
                <div class="input-group mb-2 mr-sm-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><span data-feather="user"></span></span>
                    </div>
                    <input type="text" class="form-control" id="taxRegistryNumber" placeholder="Tax Registry Number" name="ownerTaxRegistryNumber">
                </div>

                <button type="submit" class="btn btn-primary mb-2">Search</button>
            </form>
            </form>
            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <tbody>
                    <#if repairs??>
                        <#list repairs as repair>
                            <tr data-id="${repair.id}">
                                <td>${repair.date}</td>
                                <td>${repair.status}</td>
                                <td>${repair.fixType}</td>
                                <td>${repair.cost}</td>
                                <td>${repair.address}</td>
                                <td>${repair.ownerTaxRegistryNumber}</td>
                                <td>${repair.description}</td>
                                <td class="text-center"><a href="/admin/repairs/${repair.id}/edit"><span data-feather="tool"></span></a></td>
                                <td class="text-center"><button class="btn btn-link" data-toggle="modal" data-target="#modal" data-id="${repair.id}"><span data-feather="x"></span></button></td>
                            </tr>
                        </#list>
                    </#if>
                    </tbody>
                </table>
                <div class="modal" tabindex="-1" role="dialog" id="modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Repair</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Are you sure?</p>
                            </div>
                            <div class="modal-footer">
                                <form method="post" id="deleteFormRepair">
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>
    </div>
</div>

<#include "partials/scripts.ftlh">

<script src="/scripts/main.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
</body>
</html>
