jQuery(function ($) {

    var $datePicker = $('#datepicker');
    if ($datePicker.datepicker) {
        $datePicker.datepicker({format: 'yyyy-mm-dd'});
    }

    $('#modal').on('show.bs.modal', function (event) {
        const taxRegistryNumber = event.relatedTarget.dataset.id;
        $('#deleteFormOwner').attr('action', `/admin/owners/${taxRegistryNumber}/delete`);
        $('.modal-title').text(function () {
            return `Owner#${taxRegistryNumber}`;
        });
    });

    $('#modal').on('show.bs.modal', function (event) {
        const id = event.relatedTarget.dataset.id;
        $('#deleteFormRepair').attr('action', `/admin/repairs/${id}/delete`);
        $('.modal-title').text(function () {
            return `Repair#${id}`;
        });
    });

});



