package com.codehub.projectfuture.webapp.enums;

public enum PropertyType {
    SINGLE_APARTMENT("Single Apartment"),
    COTTAGE("Cottage"),
    BLOCK_OF_FLATS("Block of Flats");

    private String fullName;

    PropertyType(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }
}
