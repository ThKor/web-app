package com.codehub.projectfuture.webapp.domain;

import com.codehub.projectfuture.webapp.enums.FixType;
import com.codehub.projectfuture.webapp.enums.Status;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Optional;

@Entity
@Table(name = "REPAIR")
public class Repair {

    @Id
    @Column(name = "repair_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "datetime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status;

    @Enumerated(EnumType.STRING)
    @Column(name = "fixType", nullable = false)
    private FixType fixType;

    @Column(name = "cost")
    private Double cost;

    @Column(name = "address")
    private String address;

    @ManyToOne(optional = false, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "taxRegistryNumber")
    private Owner owner;

    @Column(name = "description")
    private String description;

    public Repair(Long id, LocalDate date, Status status, FixType fixType, Double cost, String address, Owner owner, String description) {
        this.id=id;
        this.date = date;
        this.status = status;
        this.fixType = fixType;
        this.cost = cost;
        this.address = address;
        this.owner = owner;
        this.description = description;
    }

    public Repair() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public FixType getFixType() {
        return fixType;
    }

    public void setFixType(FixType fixType) {
        this.fixType = fixType;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        final StringBuilder sb=new StringBuilder("Repair {");
        sb.append("id=").append(id);
        sb.append("dateTime=").append(date);
        sb.append(", status='").append(status).append('\'');
        sb.append(", fixType='").append(fixType).append('\'');
        sb.append(", cost='").append(cost).append('\'');
        sb.append(", address='").append(address).append('\'');
        sb.append(", owner='").append(owner).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
