package com.codehub.projectfuture.webapp.service;

import com.codehub.projectfuture.webapp.domain.Owner;
import com.codehub.projectfuture.webapp.domain.Repair;
import com.codehub.projectfuture.webapp.model.OwnerModel;
import com.codehub.projectfuture.webapp.model.RepairModel;

import java.util.List;
import java.util.Optional;

public interface OwnerService {

    Owner createOwner(Owner owner);

    Owner updateOwner(OwnerModel ownerModel);

    void deleteById(Long id);

    void deleteByTaxRegistryNumber(Long taxRegistryNumber);

    List<OwnerModel> findAll();


    List<OwnerModel> findOwnerByTaxRegistryNumber(Long taxRegistryNumber);


    Owner findByTaxRegistryNumber(Long taxRegistryNumber);

    List<OwnerModel> findOwnerByEmail(String email);

    Owner findByEmail(String email);



}

