package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.model.RepairModel;
import com.codehub.projectfuture.webapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class RepairsUserController {

    @Autowired
    private RepairService repairService;

    @GetMapping(value = "/repairs")
    public String repairs(ModelMap model) {
        List<RepairModel> repairs=repairService.findByOwner(SecurityContextHolder.getContext().getAuthentication().getName());
        model.addAttribute("repairs", repairs);
        return "repairsuser";
    }
}
