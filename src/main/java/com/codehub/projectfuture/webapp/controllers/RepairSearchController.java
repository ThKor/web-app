package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.forms.SearchRepairForm;
import com.codehub.projectfuture.webapp.model.RepairModel;
import com.codehub.projectfuture.webapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("admin")
public class RepairSearchController {

    private static final String REPAIRS_LIST = "repairs";

    @Autowired
    private RepairService repairService;

    @GetMapping(value = "/searchRepairs")
    public String searchRepairs(ModelMap modelmap) {
        modelmap.addAttribute("searchRepairForm", new SearchRepairForm());
        return "repairSearch";
    }

    @PostMapping(value = "/searchRepairs")
    public String searchRepairs(@ModelAttribute("searchRepairForm") SearchRepairForm searchRepairForm, ModelMap modelmap) {
        try {
            String afm = searchRepairForm.getOwnerTaxRegistryNumber().toString();
            if (searchRepairForm.getDate_one().isEmpty() && searchRepairForm.getDate_two().isEmpty() && !afm.isEmpty())  //backend checks for emety fields
            {
                Long taxRegistryNumber = Long.parseLong(searchRepairForm.getOwnerTaxRegistryNumber());
                List<RepairModel> repairs = repairService.findByOwnerTaxRegistryNumber(taxRegistryNumber);
                if (!repairs.isEmpty()) {
                    modelmap.addAttribute(REPAIRS_LIST, repairs);
                    return "repairs";
                }
            } else if (searchRepairForm.getDate_two().isEmpty() && afm.isEmpty() && !searchRepairForm.getDate_one().isEmpty()) {
                LocalDate date_one = LocalDate.parse(searchRepairForm.getDate_one(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                List<RepairModel> repairs = repairService.findByDate(date_one);
                if (!repairs.isEmpty()) {
                    modelmap.addAttribute(REPAIRS_LIST, repairs);
                    return "repairs";
                }
            } else if (!searchRepairForm.getDate_one().isEmpty() && !searchRepairForm.getDate_two().isEmpty() && afm.isEmpty()) {
                LocalDate date_one = LocalDate.parse(searchRepairForm.getDate_one(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                LocalDate date_two = LocalDate.parse(searchRepairForm.getDate_two(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                List<RepairModel> repairs = repairService.findByDateBetween(date_one, date_two);
                if (!repairs.isEmpty()) {
                    modelmap.addAttribute(REPAIRS_LIST, repairs);
                    return "repairs";
                }
            } else if (searchRepairForm.getDate_one().isEmpty() && searchRepairForm.getDate_two().isEmpty() && afm.isEmpty()) {
                return "repairSearch";
            }
        }catch (NumberFormatException ex)
        {
            return "repairSearch";
        }
        return "repairs";
    }

}