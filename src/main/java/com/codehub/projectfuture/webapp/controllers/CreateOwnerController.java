package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.domain.Owner;
import com.codehub.projectfuture.webapp.enums.PropertyType;
import com.codehub.projectfuture.webapp.enums.UserRole;
import com.codehub.projectfuture.webapp.forms.OwnerForm;
import com.codehub.projectfuture.webapp.mapper.OwnerFormToOwnerMapper;
import com.codehub.projectfuture.webapp.service.OwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

import static com.codehub.projectfuture.webapp.utils.GlobalAttributes.ERROR_MESSAGE;

@Controller
@RequestMapping("admin")
public class CreateOwnerController {

    private static final String OWNERS_FORM = "ownersForm";
    private static final String OWNERS_TYPE="ownersType";
    private static final String OWNERS_ROLE="ownersRole";

    @Autowired
    private OwnerService ownerService;

    @Autowired
    private OwnerFormToOwnerMapper mapper;


    @GetMapping(value = "/owners/create")
    public String createOwners(Model model) {
        model.addAttribute(OWNERS_FORM, new OwnerForm());
        model.addAttribute(OWNERS_TYPE, PropertyType.values());
        model.addAttribute(OWNERS_ROLE, UserRole.values());
        return "/pages/owners_create";
    }


    @PostMapping(value = "/owners/create")
    public String createOwners(Model model,
                               @Valid @ModelAttribute(OWNERS_FORM) OwnerForm ownerForm,
                               BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            model.addAttribute(ERROR_MESSAGE, "an error occured");
            return "pages/owners_create";
        }

        Owner owner=mapper.toOwner(ownerForm);
        ownerService.createOwner(owner);
        return "redirect:/admin/owners";
    }
}
