package com.codehub.projectfuture.webapp.controllers;

import com.codehub.projectfuture.webapp.model.RepairModel;
import com.codehub.projectfuture.webapp.service.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Controller
@RequestMapping("admin")
public class HomeController {
    private static final String REPAIRS_LIST = "repairs";

    @Autowired
    private RepairService repairService;


    @GetMapping(value = "/home")
    public String repairs(Model model) {
        DateTimeFormatter dateTimeFormatter=DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date=LocalDate.parse("2019-08-09", dateTimeFormatter);
        List<RepairModel> repairs = repairService.findByDate(date);
        model.addAttribute(REPAIRS_LIST, repairs);
        return "admin_home";
    }


}