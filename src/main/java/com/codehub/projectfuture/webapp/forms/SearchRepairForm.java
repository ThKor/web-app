package com.codehub.projectfuture.webapp.forms;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class SearchRepairForm {

    private String date_one;
    private String date_two;
    private String ownerTaxRegistryNumber;

    public String getDate_one() {
        return date_one;
    }

    public void setDate_one(String date_one) {
        this.date_one = date_one;
    }

    public String getDate_two() {
        return date_two;
    }

    public void setDate_two(String date_two) {
        this.date_two = date_two;
    }

    public String getOwnerTaxRegistryNumber() {
        return ownerTaxRegistryNumber;
    }

    public void setOwnerTaxRegistryNumber(String ownerTaxRegistryNumber) {
        this.ownerTaxRegistryNumber = ownerTaxRegistryNumber;
    }
}
