package com.codehub.projectfuture.webapp.model;

import com.codehub.projectfuture.webapp.enums.PropertyType;

public class OwnerModel {

    private String taxRegistryNumber;
    private String firstName;
    private String lastName;
    private String address;
    private String email;
    private String phoneNumber;
    private String type;
    private String role;


    public String getTaxRegistryNumber() {
        return taxRegistryNumber;
    }

    public void setTaxRegistryNumber(String taxRegistryNumber) {
        this.taxRegistryNumber = taxRegistryNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String  getLastName() {
        return lastName;
    }

    public void setLastName(String  lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String  getEmail() {
        return email;
    }

    public void setEmail(String  email) {
        this.email = email;
    }

    public String  getPhoneNumber() {return phoneNumber;}

    public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}

    public String getType() {return type;}

    public void setType(String type) { this.type = type; }

    public String getRole() {return role;}

    public void setRole(String role) {this.role =role;}

    public OwnerModel() {
    }

    public OwnerModel(String role, String taxRegistryNumber, String firstName, String lastName, String address, String email, String phoneNumber, String type) {
        this.taxRegistryNumber = taxRegistryNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.type = type;
        this.role =role;

    }

}
